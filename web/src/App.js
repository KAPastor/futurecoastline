import React, { useState, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url("http://futurecoastline.space:5000/static/F.png")',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));


export default function App() {
  const classes = useStyles();
  const [count, setCount] = useState(0);
  const [rm, setRM] = useState('');
  const [myimage, setMyImage] = useState(0);
  const [display_download, setDisplayDownload] = useState("none");
  const [latitude, setLatitude] = useState("");
  const [longitude, setLongitude] = useState("");
  const [status, setStatus] = useState("");
  const [downloadLink, setDownloadLink] = useState("");


   function fetchData(count){
     var url_req_string = "http://138.197.128.200:5000/api/v1/?latlng=" + latitude + "," + longitude;
     setStatus("Sending the request!")

     console.log(url_req_string);
     fetch(url_req_string)
        .then(res => res.json())
        .then(
          (result) => {
            console.log(result);
            setStatus("Job is processing")

            // Check the status
            var statusCheck = setInterval(function(){
              fetch("http://138.197.128.200:5000/api/v1/status?job_id="+result)
                 .then(res => res.json())
                 .then(
                   (result) => {
                     setStatus(result.message)
                     if (result.status == "DONE"){
                       setStatus("Image is ready to download")

                       clearInterval(statusCheck);
                       setDisplayDownload("block");
                       setDownloadLink("http://138.197.128.200:5000/" + result.message);
                     }else if (result.status == "FAILURE") {
                       clearInterval(statusCheck);
                     }
                   },
                   (error) => {
                     setStatus("Status failed!")

                     clearInterval(statusCheck);

                   }
                 );
            },3000);



            // setMyImage('http://localhost:5000/' + result)


          },

          (error) => {
            console.log(error);
            setStatus("Yikes that went all sorts of wrong!")

          }
        );
  };


  useEffect(() => {
      // Update the document title using the browser API
      document.title = `You clicked ${count} times`;
    });




  return (
    <React.Fragment>
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>

          <Typography component="h1" variant="h5">
            {status}
          </Typography>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="Latitude"
              label="Latitude"
              name="Latitude"
              autoComplete="Latitude"
              autoFocus
              value={latitude}
              onChange={(event)=> setLatitude(event.target.value)}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="Longitude"
              label="Longitude"
              name="Longitude"
              autoComplete="Longitude"
              autoFocus
              value={longitude}
              onChange={(event)=> setLongitude(event.target.value)}
            />

            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              onClick={() => fetchData(count)}
            >
              Submit
            </Button>

            <Box
                  component={Button}
                  type="submit"
                  fullWidth
                  variant="contained"
                  color="primary"
                  className={classes.submit}
                  display={display_download}
                  href={downloadLink}
                  target="_blank"
                >
                  Download
              </Box>
        </div>
      </Grid>
    </Grid>
    </React.Fragment>
  );
}
