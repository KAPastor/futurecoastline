from flask import Flask, render_template
from flask import request
import os
import mercantile
import requests
import shutil
from PIL import Image
import math
from os import listdir
from os.path import isfile, join
import numpy as np
from flask_cors import CORS, cross_origin
from flask import jsonify, make_response
import json

# Define the public folder
PUBLIC_FOLDER = os.path.join('static', '')

def extractRawImages(jobID,latlng)

	delta = 0.05
	tl = [latlnglatlng[0]+delta, latlng[1]-delta]
	br = [latlng[0]-delta, latlng[1]+delta]
	z = 13 # Set the resolution

	tl_tiles = mercantile.tile(tl[1],tl[0],z)
	br_tiles = mercantile.tile(br[1],br[0],z)

	x_tile_range = [tl_tiles.x,br_tiles.x];print(x_tile_range)
	y_tile_range = [tl_tiles.y,br_tiles.y];print(y_tile_range)


	for i,x in enumerate(range(x_tile_range[0],x_tile_range[1]+1)):
	    for j,y in enumerate(range(y_tile_range[0],y_tile_range[1]+1)):
	        print(x,y)
	        r = requests.get('https://api.mapbox.com/v4/mapbox.terrain-rgb/'+str(z)+'/'+str(x)+'/'+str(y)+'@2x.pngraw?access_token=pk.eyJ1Ijoia2FwYXN0b3IiLCJhIjoiY2p3eTg3eWJoMG1jZjQ4bzZmcGg5c3F3cSJ9.vhyCyD9xDDGP9EQnhB9xtA', stream=True)
	        if r.status_code == 200:
	            with open('./elevation_images/' + str(i) + '.' + str(j) + '.png', 'wb') as f:
	                r.raw.decode_content = True
	                shutil.copyfileobj(r.raw, f)

	        r = requests.get('https://api.mapbox.com/v4/mapbox.satellite/'+str(z)+'/'+str(x)+'/'+str(y)+'@2x.png?access_token=pk.eyJ1Ijoia2FwYXN0b3IiLCJhIjoiY2p3eTg3eWJoMG1jZjQ4bzZmcGg5c3F3cSJ9.vhyCyD9xDDGP9EQnhB9xtA', stream=True)
	        if r.status_code == 200:
	            with open('./satellite_images/' + str(i) + '.' + str(j) + '.png', 'wb') as f:
	                r.raw.decode_content = True
	                shutil.copyfileobj(r.raw, f)




	for img_name in ['elevation','satellite']:
	    image_files = ['./'+img_name+'_images/' + f for f in listdir('./'+img_name+'_images/')]
	    images = [Image.open(x) for x in image_files]

	    edge_length_x = x_tile_range[1] - x_tile_range[0]
	    edge_length_y = y_tile_range[1] - y_tile_range[0]
	    edge_length_x = max(1,edge_length_x)
	    edge_length_y = max(1,edge_length_y)
	    width, height = images[0].size

	    total_width = width*edge_length_x
	    total_height = height*edge_length_y

	    composite = Image.new('RGB', (total_width, total_height))

	    anim_idx = 0
	    y_offset = 0
	    for i in range(0,edge_length_x):
	        x_offset = 0
	        for j in range(0,edge_length_y):
	            tmp_img = Image.open('./'+img_name+'_images/' + str(i) + '.' + str(j) + '.png')
	            composite.paste(tmp_img, (y_offset,x_offset))
	            x_offset += width


	        y_offset += height

	    composite.save('./composite_images/'+img_name+'.png')







	elevation_raw = Image.open('./composite_images/elevation.png')
	rgb_elevation = elevation_raw.convert('RGBA')

	# Loop over the image and save the data in a list:
	elevation_data = []
	# texture_data = []
	for h in range(rgb_elevation.height):
	    elev_row = []
	    for w in range(rgb_elevation.width):
	        R, G, B, A = rgb_elevation.getpixel((w, h))
	        height = -10000 + ((R * 256 * 256 + G * 256 + B) * 0.1)
	        elev_row.append(height)
	    elevation_data.append(elev_row)
	import json
	with open('./elevation.json', 'w') as outfile:
	    json.dump(elevation_data, outfile)



	# Find the location elevation
	e=np.array([np.array(xi) for xi in elevation_data])
	delta = e.max()-e.min()
	level = e.min() + delta*float(fill)


	im = Image.open('./composite_images/satellite.png').convert('RGBA')
	overlay = Image.new('RGBA', im.size,(4,22,37,255))
	ni = np.array(overlay)

	depth = level - e
	print(depth.min(),depth.max())
	# Now we have the depths  we want anything > 0 to be zero alpha
	alpha_mask = np.copy(depth)
	alpha_mask = alpha_mask*255/alpha_mask.max()
	alpha_mask  = np.where(alpha_mask<0, 0, alpha_mask)
	alpha_mask = alpha_mask**.2
	alpha_mask = alpha_mask*255/alpha_mask.max()


	ni[...,3] = alpha_mask[...]

	W = Image.fromarray(ni)
	im.paste(W , (0,0),W)
	if os.path.exists("./static/F.png"):
		os.remove('./static/F.png')
	im.save('./static/F.png')




app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['UPLOAD_FOLDER'] = PUBLIC_FOLDER

@app.route('/api/v1/')
@cross_origin()
def RequestImage():
	# Takes in lat/lng as inputs
	latlng_str = request.args.get('latlng')
	latlng = [float(i) for i in latlng_str.split(',')]
	# Do some error checking here:

	job_id = latlng_str.replace(',','_')

	# Build the temp and public directories
	os.mkdir('./temp/' + job_id)
	# Create the status json in the new directory for temp
	status = {};status['status'] = 'RUNNING';status['message'] = 'Building Directory Structure'
	with open('./temp/' + job_id + '/status.json' , 'w') as fp:
		json.dump(status, fp)

	os.mkdir('./static/' + job_id)

	# Now we can call the sub functions with the job ID
	status = {};status['status'] = 'RUNNING';status['message'] = 'Extracting sub-images from Mapbox API'
	with open('./temp/' + job_id + '/status.json' , 'w') as fp:
		json.dump(status, fp)

	extractRawImages(job_id,latlng)





# lat_lng = request.args.get('lat_lng')
# fill = request.args.get('fill')
#
# lat_lng = [float(i) for i in lat_lng.split(',')]
# generate_image(lat_lng,fill)
# full_filename = os.path.join(app.config['UPLOAD_FOLDER'], 'F.png')
# print(full_filename)
# res = make_response(jsonify(full_filename), 200)
#



	# Kicks off the run and returns the jobID
	return "JOB ID"

@app.route('/api/v1/status')
@cross_origin()
def GetStatus():
	# Takes in the jobID as inputs
	# Returns the status of the return (if complete it gives the image path)
	return "Current Status"





if __name__ == '__main__':
    app.run(host='0.0.0.0')
