from flask import Flask, render_template
from flask import request
import os
import mercantile
import requests
import shutil
from PIL import Image
import math
from os import listdir
from os.path import isfile, join
import numpy as np
from flask_cors import CORS, cross_origin
from flask import jsonify, make_response
import json
import _thread

# Define the public folder
PUBLIC_FOLDER = os.path.join('static', '')
base_dir = '/home/kapastor/futurecoastline/api'
def extractRawImages(jobID,latlng):

	delta = 0.05
	tl = [latlng[0]+delta, latlng[1]-delta]
	br = [latlng[0]-delta, latlng[1]+delta]
	z = 13 # Set the resolution

	tl_tiles = mercantile.tile(tl[1],tl[0],z);br_tiles = mercantile.tile(br[1],br[0],z)
	x_tile_range = [tl_tiles.x,br_tiles.x];y_tile_range = [tl_tiles.y,br_tiles.y];

	for i,x in enumerate(range(x_tile_range[0],x_tile_range[1]+1)):
	    for j,y in enumerate(range(y_tile_range[0],y_tile_range[1]+1)):
	        r = requests.get('https://api.mapbox.com/v4/mapbox.satellite/'+str(z)+'/'+str(x)+'/'+str(y)+'@2x.png?access_token=pk.eyJ1Ijoia2FwYXN0b3IiLCJhIjoiY2p3eTg3eWJoMG1jZjQ4bzZmcGg5c3F3cSJ9.vhyCyD9xDDGP9EQnhB9xtA', stream=True)
	        if r.status_code == 200:
	            with open(base_dir + '/temp/' + jobID + '/' + str(i) + '.' + str(j) + '.png', 'wb') as f:
	                r.raw.decode_content = True
	                shutil.copyfileobj(r.raw, f)

def generateCompositeImage(jobID,latlng):
	delta = 0.05
	tl = [latlng[0]+delta, latlng[1]-delta]
	br = [latlng[0]-delta, latlng[1]+delta]
	z = 13 # Set the resolution

	tl_tiles = mercantile.tile(tl[1],tl[0],z);br_tiles = mercantile.tile(br[1],br[0],z)
	x_tile_range = [tl_tiles.x,br_tiles.x];y_tile_range = [tl_tiles.y,br_tiles.y];


	image_files = [base_dir + '/temp/' + jobID + '/' + f for f in listdir(base_dir + '/temp/' + jobID + '/') if f.endswith(".png")]
	images = [Image.open(x) for x in image_files]
	edge_length_x = x_tile_range[1] - x_tile_range[0]
	edge_length_y = y_tile_range[1] - y_tile_range[0]
	edge_length_x = max(1,edge_length_x)
	edge_length_y = max(1,edge_length_y)
	width, height = images[0].size

	total_width = width*edge_length_x;total_height = height*edge_length_y

	composite = Image.new('RGB', (total_width, total_height))

	y_offset = 0
	for i in range(0,edge_length_x):
		x_offset = 0
		for j in range(0,edge_length_y):
			tmp_img = Image.open(base_dir + '/temp/' + jobID + '/' + str(i) + '.' + str(j) + '.png')
			composite.paste(tmp_img, (y_offset,x_offset))
			x_offset += width
		y_offset += height

	composite.save(base_dir + '/static/' + jobID + '/' + jobID +'.png')
	status = {};status['status'] = 'DONE';status['message'] = '/static/' + jobID + '/' + jobID + '.png'
	with open(base_dir + '/temp/' + jobID + '/status.json' , 'w') as fp:
		json.dump(status, fp)





app = Flask(__name__)
cors = CORS(app)
app.config['CORS_HEADERS'] = 'Content-Type'
app.config['UPLOAD_FOLDER'] = PUBLIC_FOLDER


def wrapper(job_id,latlng):
	print('Running on thread')
	try:

		# Now we can call the sub functions with the job ID
		print('extractRawImages')
		status = {};status['status'] = 'RUNNING';status['message'] = 'Extracting sub-images from Mapbox API'
		with open(base_dir + '/temp/' + job_id + '/status.json' , 'w') as fp:
			json.dump(status, fp)
		extractRawImages(job_id,latlng)

		# Generating Composites
		print('generateCompositeImage')
		status = {};status['status'] = 'RUNNING';status['message'] = 'Generating composite image'
		with open(base_dir + '/temp/' + job_id + '/status.json' , 'w') as fp:
			json.dump(status, fp)
		generateCompositeImage(job_id,latlng)
	except:
		print('Running on thread: error')

		status = {};status['status'] = 'FAILURE';status['message'] = 'Something went wrong when generating the image'
		with open(base_dir + '/temp/' + job_id + '/status.json' , 'w') as fp:
			json.dump(status, fp)
		raise


@app.route('/api/v1/')
@cross_origin()
def RequestImage():
	# Takes in lat/lng as inputs
	latlng_str = request.args.get('latlng')
	latlng = [float(i) for i in latlng_str.split(',')]
	# Do some error checking here:

	job_id = latlng_str.replace(',','_')

	# Build the temp and public directories
	if not os.path.exists(base_dir + '/temp/' + job_id):
		os.mkdir(base_dir + '/temp/' + job_id)
	# Create the status json in the new directory for temp
	status = {};status['status'] = 'RUNNING';status['message'] = 'Building directory structure'
	with open(base_dir + '/temp/' + job_id + '/status.json' , 'w') as fp:
		json.dump(status, fp)

	if not os.path.exists(base_dir + '/static/' + job_id):
		os.mkdir(base_dir + '/static/' + job_id)

	_thread.start_new_thread( wrapper, (job_id,latlng,) )

	# Kicks off the run and returns the jobID
	return make_response(jsonify(job_id), 200)

@app.route('/api/v1/status')
@cross_origin()
def GetStatus():
	# Takes in the jobID as inputs
	job_id = request.args.get('job_id')
	with open(base_dir + '/temp/' + job_id + '/status.json') as fp:
		status = json.load(fp)
	return make_response(jsonify(status), 200)


if __name__ == '__main__':
    app.run(host='0.0.0.0')
